<?php

use yii\db\Migration;

/**
 * Class m180421_135719_create_tables_book_autors
 */
class m180421_135719_create_tables_book_autors extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('books', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'date_create' => $this->dateTime()->defaultValue(0),
            'date_update' => $this->dateTime()->defaultValue(0),
            'preview' => $this->string()->notNull(),
            'date' => $this->date()->notNull(),
            'author_id' => $this->integer()->notNull()
        ]);
        $this->createTable('authors', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->notNull(),
            'lastname' => $this->string()->notNull(),
        ]);
        $this->createIndex(
            'idx-authors-id',
            'authors',
            'id'
        );
        $this->createIndex(
            'idx-books-author_id',
            'books',
            'author_id'
        );
        $this->addForeignKey(
            'fk-id-author_id',
            'books',
            'author_id',
            'authors',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-id-author_id',
            'authors'
        );

        $this->dropIndex(
            'idx-books-author_id',
            'books'
        );
        $this->dropIndex(
            'idx-authors-id',
            'authors'
        );
        $this->dropTable('authors');
        $this->dropTable('books');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180421_135719_create_tables_book_autors cannot be reverted.\n";

        return false;
    }
    */
}
