<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>books lib!</h1>

        <p class="lead">sign in to see it</p>

        <p><a class="btn btn-lg btn-success" href="<?=Url::toRoute(['/book', 'id' => 42]);?>">see my library</a></p>
    </div>

    <div class="body-content">



    </div>
</div>
